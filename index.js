// khai báo thư viện cần dùng
const express = require("express");
const app = express();
const users = require("./data.js");

//routers
const userRouter = require("./app/routes/userRouter");
app.use("", userRouter);

// khai báo cổng
const port = 8000;
app.listen(port, function() {
    console.log(`App listening on port ${port}`);
})