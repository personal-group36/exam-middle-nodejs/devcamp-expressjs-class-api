const users = require("../../data");

const router = require("express").Router();

router.get("/users", function(request, response){
    const age = request.query.age;
    if (!age || age === undefined) {
        response.json({
            message: 'Get ALL Users',
            data: users
        })
    } else {
        response.json({
            message: `Get Users with Age > ${age}`,
            data: users.filter(element => element.age > age)
        })
    }
})
router.get("/users/:id", function(request, response){
    const id = request.params.id;
    if (!id || isNaN(id) ){
        response.json({
            message: `Error 400 Bad request`
        })
    } else if (id >= users.length || id <= 0) {
        response.json({
            message: `User not found`
        })
    } else {
    response.json({
        message: `Get User with ID ${id}`,
        data: users.find(element => element.id = id)
    })}
})

router.post("/users", function(request, response){
    response.json({
        message: 'Create User'
    })
})

router.put("/users/:id", function(request, response){
    const id = request.params.id;
    response.json({
        message: `Update User with ID ${id}`
    })
})
router.delete("/users/:id", function(request, response){
    const id = request.params.id;
    response.json({
        message: `Delete User with ID ${id}`
    })
})

module.exports = router;